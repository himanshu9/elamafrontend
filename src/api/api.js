import axios from "axios";

const commonApi = "http://18.139.69.39:4000/";




export default {
  user: {
    signup: data => axios.post(`${commonApi}user/signup`, data)
      .then(res => res.data),

    loginUser: data => axios.post(`${commonApi}user/login`, data)
      .then(res => res.data),

    loginAdmin: data => axios.post(`${commonApi}admin/login`, data)
      .then(res => res.data),

    getDetails: (header) => axios.get(`${commonApi}userDetails`, header)
      .then(res => res.data),
    getBalance: (header) => axios.get(`${commonApi}balance`, header)
      .then(res => res.data),
      getUserBalance: (header,id) => axios.get(`${commonApi}userBalance/${id}`, header)
      .then(res => res.data),
     getTxHistory: (header,address=undefined) => axios.get(`${commonApi}history?walletAddress=${address}`, header)
      .then(res => res.data),
      sendPayment: (body,header) => axios.post(`${commonApi}user/sendPayment`, body, header)
      .then(res => res.data),
  },

  platform: {
    createUserAccount: (body,header) => axios.post(`${commonApi}user/createAccount`, body, header)
    .then(res => res.data),
    allUsers: (header) => axios.get(`${commonApi}users`, header)
      .then(res => res.data),
    sendToUser: (body,header) => axios.post(`${commonApi}admin/sendToUser`, body, header)
      .then(res => res.data),
    mintTokens: (body,header) => axios.post(`${commonApi}admin/mintTokens`, body, header)
      .then(res => res.data),
    createAdminAccount: (header) => axios.post(`${commonApi}admin/createAdminAccount`, {}, header)
      .then(res => res.data),
    configuration: (header) => axios.get(`${commonApi}admin/configuration`, header)
      .then(res => res.data),
    getChannels: () => axios.get(`${commonApi}channels`)
      .then(res => res.data),
    getChaincodes: () => axios.get(`${commonApi}chaincodes`)
      .then(res => res.data),
    firstDone: (header) => axios.post(`${commonApi}admin/org/addOrgUser`, { "orgName": "Org1" }, header)
      .then(res => res.data),

    secondDone: (header) => axios.post(`${commonApi}admin/org/addOrgUser`, { "orgName": "Org2" }, header)
      .then(res => res.data),

    channelCreated: (header) => axios.post(`${commonApi}admin/addChannel`, {}, header)
      .then(res => res.data),

    org1Joined: (header) => axios.post(`${commonApi}admin/joinChannel`, { "orgName": "Org1" }, header)
      .then(res => res.data),

    org2Joined: (header) => axios.post(`${commonApi}admin/joinChannel`, { "orgName": "Org2" }, header)
      .then(res => res.data),

    chaincode1: (header) => axios.post(`${commonApi}admin/installChaincode`, { "orgName": "Org1" }, header)
      .then(res => res.data),

    chaincode2: (header) => axios.post(`${commonApi}admin/installChaincode`, { "orgName": "Org2" }, header)
      .then(res => res.data),

    chaincodeInstantiated: (header) => axios.post(`${commonApi}admin/instantiateChaincode`, {}, header)
      .then(res => res.data)
  }

}