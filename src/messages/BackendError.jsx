import React from "react";
import {Message} from "semantic-ui-react";

const BackendError = ({text}) => (
    <Message negative>
    <Message.Header>Error!</Message.Header>
    <p>{text}</p>
  </Message>
);

export default BackendError;