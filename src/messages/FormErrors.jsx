import React from "react";

const FormErrors = ({text}) => <span style={{color: "#ea2520"}} >{text}</span>

export default FormErrors;