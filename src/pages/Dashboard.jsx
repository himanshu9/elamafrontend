import React from "react";
import {Grid, Menu, Icon, AccordionPanel} from "semantic-ui-react";
import Account from "./dashboardComponents/accountSection/Account";
import Payments from "./dashboardComponents/paymentSection/Payments";
import History from "./dashboardComponents/History";
import AccountDetails from "./dashboardComponents/AccountDetails";
import Navbar from "./navbar/Navbar";

class Dashboard extends React.Component {

    state = {
        activeItem: "account",
        userDetails:{
            username:'user'
        }
    }

    handleItemClick = (event, {name}) => {
        this.setState({activeItem: name})
    }

    handleUserDetails = (userDetails)=>{
       this.setState({
           userDetails
       })
    }

    render () {
        const {activeItem} = this.state;
        return (
            <div>
            <Navbar history={this.props.history} userDetails={this.state.userDetails}/>
            <Grid>
               
                <Grid.Column stretched textAlign="center" style={{marginTop: "10vh"}} >
                 <Account handleUserDetails={this.handleUserDetails}/>
                </Grid.Column>
            </Grid>
            </div>
        )
    }
}

export default Dashboard;