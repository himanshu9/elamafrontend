import React from "react";
import validator from "validator";
import {Menu, Segment, Form, Button, Icon, Grid, Input} from "semantic-ui-react";
import FormErrors from "../messages/FormErrors";
import api from "../api/api";
import BackendError from "../messages/BackendError";

class LoginPage extends React.Component {

    state = {
        activeItem: "user",
        data: {
            email: "",
            password: ""
        },
        loading: false,
        showPassword: false,
        errors: {}
    }

    change = (event) => {
        this.setState({
            data: {...this.state.data, [event.target.name]: event.target.value}
        })
    }

    handleClick = (e, {name}) => this.setState({activeItem: name})

    submit = () => {
        const errors = this.validate(this.state.data);
        this.setState({errors});
        if (Object.keys(errors).length === 0) {
            if (this.state.activeItem === "user") {
                api.user.loginUser(this.state.data)
                .then((data) => {
                     if (data.success) {
                        localStorage.setItem("token", data.authToken);
                        this.props.history.push("/dashboard");  
                    } 
                })
                .catch(err => this.setState({...this.state, errors: {global: err.response.data.message}}))
            }
            else if (this.state.activeItem === "admin") {
                api.user.loginAdmin(this.state.data)
                 .then(data => {
                     if (data.success) {
                         localStorage.setItem("token", data.authToken);
                         this.props.history.push("/admindashboard");
                     }
                     else this.setState({...this.state, errors: {global: data.message}})
                 })
                 .catch(err => this.setState({...this.state, errors: {global: err.response.data.message}}))
            }
        }
    }

    signup=()=>{
        this.props.history.push("/signup");
    }

    validate = data => {
        const errors = {};
        if (!validator.isEmail(data.email))
          errors.email = "Please Enter a valid Email!";
        if (validator.isEmpty(data.email))
          errors.email = "Email field can't be left empty!";
        if (!validator.isLength(data.password, { min: 8, max: 30 }))
          errors.password =
            "Password Length should be between 8 and 30 Characters!";
        if (validator.isEmpty(data.password))
          errors.password = "Password field can't be left blank!";
        return errors;
      };
    

    showPassword = () => this.setState({showPassword: !this.state.showPassword});

    render () {
        const {data, activeItem, showPassword, errors} = this.state;
       
        const h1Styles = {
            marginLeft: "46vw",
            marginTop: "5vh"
        }

        const segmentStyles = {
            marginTop: "10vh"
        }

        return (
            <div className="ui inverted vertical masthead center aligned segment">
              
                <Grid>

                    <Grid.Column width="5" ></Grid.Column>
                    <Grid.Column width="6" >
                        <Segment clearing raised style={segmentStyles} >
                        <h1 >Login</h1>
                        <Button primary floated="Left" onClick={this.signup}>
                        New User?
                      </Button>
                      <Menu widths={2} pointing secondary >
                          <Menu.Item name="user" active={activeItem === "user"} onClick={this.handleClick} >For User</Menu.Item>
                          <Menu.Item name="admin" active={activeItem === "admin"} onClick={this.handleClick} >For Admin</Menu.Item>
                      </Menu>
                            <Form onSubmit = {this.submit} size="large" >
                                {errors.global && <BackendError text={errors.global} /> }
                                <Form.Field>
                                    <label >Email:</label>
                                    <input type="text"
                                    placeholder = "Enter Email"
                                    name = "email"
                                    value = {data.email}
                                    onChange =  {this.change}
                                    />
                     {errors.email && <FormErrors text={errors.email} />}
                                </Form.Field>
                                <Form.Field>
                                <label >Password:</label>
                                <Input type= {showPassword ? "text" : "password"} 
                                placeholder="Enter Password"
                                 icon={<Icon className='eye slash' link 
                                 onClick={this.showPassword}/>}
                                 name = "password"
                                value = {data.password}
                                onChange = {this.change}
                                 />
                     {errors.password && <FormErrors text={errors.password} />}
                                </Form.Field>
                              
                                <Button floated="right" primary>Login</Button>
                               
                            </Form>
                          
                        </Segment>
                    </Grid.Column>
                    <Grid.Column width="3" ></Grid.Column>
                </Grid>  
            </div>       
        )
    }
}

export default LoginPage;