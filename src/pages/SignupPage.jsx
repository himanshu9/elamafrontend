import React from "react";
import validator from "validator";
import { Segment, Form, Button, Grid, Input, Icon, Dropdown } from "semantic-ui-react";
import FormErrors from "../messages/FormErrors";
import api from "../api/api";
import BackendError from "../messages/BackendError";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class SignupPage extends React.Component {
  state = {
    data: {
      email: "",
      password: "",
      orgName: "Org1"
    },
    errors: {},
    showPassword: false
  };

  change = event => {
    this.setState({
      data: { ...this.state.data, [event.target.name]: event.target.value }
    });
  };

  dropdownChange = (e, {value}) => {
    this.setState({
      data: {...this.state.data, orgName: value}
    })
  }

  submit = () => {
    const errors = this.validate(this.state.data);
    this.setState({ errors });
    if (Object.keys(errors).length === 0) {
      api.user.signup(this.state.data)
      .then(data => {
        if (data.success) {
          NotificationManager.success(data.message)
          setTimeout(()=>{
            this.props.history.push("/login")
          },2000)
         
        } 
        else this.setState({...this.state, errors: {global: data.message}})
      })
      .catch(err => this.setState({...this.state, errors: {global: err.response.data.message}}))
    }
  };

  validate = data => {
    const errors = {};
    if (!validator.isEmail(data.email))
      errors.email = "Please Enter a valid Email!";
    if (validator.isEmpty(data.email))
      errors.email = "Email field can't be left empty!";
    if (!validator.isLength(data.password, { min: 8, max: 30 }))
      errors.password =
        "Password Length should be between 8 and 30 Characters!";
    if (validator.isEmpty(data.password))
      errors.password = "Password field can't be left blank!";
    return errors;
  };

  showPassword = () =>
    this.setState({ showPassword: !this.state.showPassword });

    login=()=>{
      
       
        this.props.history.push("/login");
    
    }

    

  render() {
    const { data, showPassword, errors } = this.state;

    const options = [
      { key: 'or1', text: 'Org1', value: 'Org1' },
      { key: 'org2', text: 'Org2', value: 'Org2' },
    ]

    const segmentStyles = {
      marginTop: "10vh"
    };

    const h1Styles = {
      marginLeft: "45vw",
      marginTop: "5vh"
    };

    return (
      <div className="ui inverted vertical masthead center aligned segment">
       
        <Grid>
          <Grid.Column width="5" />
          <Grid.Column width="6">
            <Segment raised clearing style={segmentStyles}>
            <h1 >Sign Up</h1>
            <Button primary floated="center" onClick={this.login}>
            Already Registered? 
          </Button>
              <Form onSubmit={this.submit} size="large">
                {errors.global && <BackendError text={errors.global} /> }
                <Form.Field error={!!errors.email}>
                  <label>Email:</label>
                  <input
                    type="text"
                    placeholder="Enter Email"
                    name="email"
                    value={data.email}
                    onChange={this.change}
                  />
                  {errors.email && <FormErrors text={errors.email} />}
                </Form.Field>
                <Form.Field error={!!errors.password}>
                  <label>Password:</label>
                  <Input
                  iconPosition="left"
                    placeholder="Enter Password"
                    type={showPassword ? "text" : "password"}
                    icon={
                      <Icon
                      className="eye slash"
                      link
                      onClick={this.showPassword}
                      />
                    }
                    action={<Dropdown button basic
                       floating options={options}
                       onChange = {this.dropdownChange}
                      defaultValue='Org1' />}
                    name="password"
                    value={data.password}
                    onChange={this.change}
                  />
                  {errors.password && <FormErrors text={errors.password} />}
                </Form.Field>
               
                <Button primary floated="right">
                  Signup
                </Button>
              </Form>
            </Segment>
          </Grid.Column>
        </Grid>
        <NotificationContainer leaveTimeout={200}/>
      </div>
    );
  }
}

export default SignupPage;
