import React from "react";
import { Grid, Menu, Icon } from "semantic-ui-react";
import Navbar from "../navbar/Navbar";
import Channels from "./channelsSection/Channels";
import ChainCodes from "./chainCodesSection/ChainCodes";
import Accounts from "./accountsSection/Account";
import ConfigurationSteps from "./configurationSection/ConfigurationSteps";
import Users from './users'
class AdminDashboard extends React.Component {

  state = {
    activeItem: "configuration",
    isOrg2: false,
    userDetails:{
      username:'admin@elamachain.io'
  }
  };

  handleItemClick = (event, { name }) => {
    this.setState({ activeItem: name });
  };

  handleNavDropdown = event => {
    if (event === 3 || event === 5) this.setState({ isOrg2: true });
    else this.setState({ isOrg2: false });
  };

  render() {

    const { activeItem } = this.state;

    return (
      <div>
        <Navbar isOrg2={this.state.isOrg2} history={this.props.history} userDetails={this.state.userDetails} />
        <Grid>
          <Grid.Column width={3}>
            <Menu fluid vertical tabular>
            
              <Menu.Item
                name="configuration"
                active={activeItem === "configuration"}
                onClick={this.handleItemClick}
              >
                Configuration
                <Icon name="configure" />
              </Menu.Item>
              <Menu.Item
              name="accounts"
              active={activeItem === "accounts"}
              onClick={this.handleItemClick}
            >
              Accounts
              <Icon name="address book" />
            </Menu.Item>
                <Menu.Item
              name="users"
              active={activeItem === "users"}
              onClick={this.handleItemClick}
            >
              Users
              <Icon name="address book" />
            </Menu.Item>
             
            
            </Menu>
            
          </Grid.Column>
          <Grid.Column
            width={12}
            stretched
            textAlign="center"
            style={{ marginTop: "10vh" }}
          >
          {activeItem === "users" && <Users />}
            {activeItem === "accounts" && <Accounts />}
            {activeItem === "configuration" && (
              <ConfigurationSteps  history={this.props.history} handleNavDropdown={this.handleNavDropdown} />
            )}
          </Grid.Column>
        </Grid>
      </div>
    );
  }
}

export default AdminDashboard;
