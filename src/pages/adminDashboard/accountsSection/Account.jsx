import React from "react";
import {Button, Icon} from "semantic-ui-react";

import AccountDetails from "./balance";
import api from "../../../api/api";
import Loader from './../../loader';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class Account extends React.Component {

  componentDidMount(){
      this.setState({
          detailsLoading:true
      })
      const headers = {
        headers: {
          'x-auth-token': localStorage.getItem('token'),
        }
       
      };
      
      console.log("db header",headers)
    api.user.getDetails(headers)
    .then(data => {
       if (data.success) {
        
        if(data.data.wallet){
            this.setState({
                isPriorAccount:true,
                walletAddress:data.data.walletAddress
            })
        } else {
            this.setState({
                isPriorAccount:false
            })
        }

        this.setState({
            userData:data,
            detailsLoading:false
        })
        
       } else {
     
        this.setState({
            detailsLoading:false
        })
       }

    }).catch((err)=>{
       
        this.setState({
            detailsLoading:false
        })
    })
  }

    state = {
        isPriorAccount: false,
        isFormOpen: false,
        detailsLoading:false,
        walletAddress:'',
        userData:{
            wallet:false
        },
    }

    createAdminAccount = ()=>{
        this.setState({
            detailsLoading:true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.createAdminAccount(headers)
    .then(data => {
       if (data.success) {      
      
        api.user.getDetails(headers)
        .then(data => {
           if (data.success) {
              
            if(data.data.wallet){
                this.setState({
                    isPriorAccount:true,
                    walletAddress:data.data.walletAddress
                })
            } else {
                this.setState({
                    isPriorAccount:false
                })
            }
    
            this.setState({
                userData:data,
                detailsLoading:false
            })
            NotificationManager.success(data.message);
           } else {
            this.setState({
                detailsLoading:false
            })
           }
    
        }).catch((err)=>{
            this.setState({
                detailsLoading:false
            })
        })
       } else {
       
        this.setState({
            detailsLoading:false
        })
        NotificationManager.error(data.message);
       }

    }).catch((err)=>{
      
        this.setState({
            detailsLoading:false
        })
       
    })
    }

    render () {
        const {isFormOpen, isPriorAccount} = this.state;

        return (
          <div>
          {
              this.state.detailsLoading ? <Loader/> :
              <div>
              {!isPriorAccount && <div>
               <Button icon labelPosition="right" onClick = {this.createAdminAccount} style={{marginTop: "10px"}} >Create Account
              <Icon className="add user"/>
              </Button>
           
              </div>
              }
              {isPriorAccount && <AccountDetails walletAddress={this.state.walletAddress}/>}
              </div>  
          }
          <NotificationContainer/>
          </div>  
        )
    }
}

export default Account;