import React from "react";
import { Grid, Segment, Menu } from "semantic-ui-react";
import ReactTable from 'react-table';
import { Button, Header, Image, Modal, Icon, Input, Dropdown } from "semantic-ui-react";
import api from "../../../api/api";
import Loader from './../../loader';
import Pin from '../../pin';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class AccountDetails extends React.Component {

    state = {
        balance: "0",
        activeItem: "account",
        loading: false,
        mintModel: false,
        sendModel: false,
        users: [],
        history:[],
        toAddress:''

    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        this.getBalance(headers);
        this.getUsers(headers);

    }

    getHistory = () => {
        this.setState({
            loading:true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        let walletAddress = !!this.props.walletAddress ? this.props.walletAddress : undefined
        api.user.getTxHistory(headers,walletAddress)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        history: data.data

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getBalance = (headers) => {
        api.user.getBalance(headers)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        balance: data.balance

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getUsers = (headers) => {
        api.platform.allUsers(headers)
            .then(data => {

                if (data.success) {

                    let users = data.data;
                    console.log(users)
                    let modifiedUsers = [];

                    users.map((user) => {
                        let modifiedUser = {};
                        modifiedUser.key = user.email;
                        modifiedUser.text = user.email;
                        modifiedUser.value = user.walletAddress;

                        modifiedUsers.push(modifiedUser);
                    })
                    this.setState({
                        loading: false,
                        users: modifiedUsers

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    handleClick = (event, { name }) => {
        this.setState({ activeItem: name })
        if(name=== 'history') {
            this.getHistory();
        }
        
    }

    mintTokens = () => {

        console.log(this.state.mintTokensNumber)

        if(this.state.mintTokensNumber <= 0) {
            NotificationManager.error("Token number should be greater than zero");
        } else {
            this.setState({
                loading: true
            })
            let body = {
                amount: this.state.mintTokensNumber
            }
            const headers = {
                headers: {
                  'x-auth-token': localStorage.getItem('token'),
                }
               
              };
    
            api.platform.mintTokens(body,headers)
                .then(data => {
    
                    if (data.success) {
                        this.setState({
                            loading: false,
                            mintModel: false
                        })
                        this.getBalance(headers);
                        NotificationManager.success(data.message);
                    } else {
                        this.setState({
                            loading: false
                        })
                        NotificationManager.error(data.message);
                    }
    
                }).catch((err) => {
                    this.setState({
                        loading: false
                    })
                   
                })
        }

      

    }

    sendTokens = () => {

        this.setState({
            loading:true
        })
        if(this.state.sendTokensNumber <= 0) {
            NotificationManager.error("Token number should be greater than zero");
        } else {

        let body = {
            amount: this.state.sendTokensNumber,
            toAddress:this.state.toAddress
        }
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.sendToUser(body,headers)
        .then(data => {

            if (data.success) {
                this.setState({
                    loading: false,
                    sendModel: false
                })
                this.getBalance(headers);
                NotificationManager.success(data.message);
            } else {
                this.setState({
                    loading: false
                })
                NotificationManager.error(data.message);
            }

        }).catch((err) => {
            this.setState({
                loading: false
            })
           
        })

    }
    }

    toggleMintModal = () => {
        this.setState({
            mintModel: !this.state.mintModel
        })
    }

    sendModel = () => {
        this.setState({
            sendModel: !this.state.sendModel
        })
    }

    changeMintTokens = (e) => {
        this.setState({
            mintTokensNumber: e.target.value
        })
    }

    changeSendTokens = (e) => {
        this.setState({
            sendTokensNumber: e.target.value
        })
    }
   
     handleOnChange = (e, data) => {
        console.log(data.value);
        this.setState({
            toAddress:data.value
        })
     }
  
    render() {

        console.log("history",this.state.history)

        const segmentStyles = {
            marginLeft: "5vw"
        }

        let { activeItem } = this.state

       
        const columns = [
            {
                Header: "To",
                accessor: "to"
            },
            {
                Header: "Action",
                accessor: "action"
            },
            {
                Header: "Amount",
                accessor: "amount"
            },
            {
                Header: "TxId",
                accessor: "txid"
            }
        ];

        const tableStyles = {
            marginLeft: "2vw"
        }



        return (


            this.state.loading ? <Loader /> :
                <Segment style={segmentStyles} raised >
                    <Menu widths={2} pointing secondary >
                        <Menu.Item name="account"
                            active={activeItem === "account"}
                            onClick={this.handleClick}
                            color={activeItem === "account" ? "blue" : "black"}
                        >Wallet
   </Menu.Item>
                        <Menu.Item name="history"
                            active={activeItem === "history"}
                            onClick={this.handleClick}
                            color={activeItem === "history" ? "blue" : "black"}
                        >Transaction History
   </Menu.Item>
                    </Menu>
                    {activeItem === "history" && <ReactTable data={this.state.history} columns={columns} style={tableStyles} defaultPageSize={11} />}
                    {activeItem === "account" &&




                        <div>

                            <h1 style={{ color: "green" }} >Wallet Address:</h1>
                            <h4 style={{ color: "green" }} >{this.props.walletAddress}</h4>




                            <h1 style={{ color: "green" }} >Balance:</h1>
                            <h4 style={{ color: "green" }} >{this.state.balance}</h4>

                            <Modal open={this.state.mintModel} trigger={<Button icon labelPosition="right" onClick={this.toggleMintModal} style={{ marginTop: "10px" }} >Mint Tokens
     <Icon className="add user" />
                            </Button>}>
                                <Modal.Header>Mint Tokens</Modal.Header>
                                <Icon className="close" onClick={()=>(this.setState({mintModel:false}))}/>
                                <Modal.Content image>
                                  
                                    <Modal.Description>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: 20 }}>


                                            <Input style={{width:800}} type="number" size='massive' onChange={this.changeMintTokens} value={this.state.mintTokensNumber} placeholder="Enter Number of tokens to mint" />

                                            <Button icon labelPosition="right" style={{ margin: "10px" }} onClick={this.mintTokens} >Mint
       <Icon className="add user" />
                                            </Button>

                                        </div>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>





                            <Modal open={this.state.sendModel} trigger={<Button icon labelPosition="right" style={{ marginTop: "10px" }}  onClick={()=>(this.setState({sendModel:true}))}>Send Tokens
     <Icon className="add user" />
                            </Button>}>
                                <Modal.Header>Send Tokens</Modal.Header>
                                <Icon className="close" onClick={()=>(this.setState({sendModel:false}))}/>
                                <Modal.Content image>
                                   
                                    <Modal.Description>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: 20 }}>


                                            <Input type="number" style={{width:800}} size='huge' onChange={this.changeSendTokens} value={this.state.sendTokensNumber} placeholder="Enter Number of tokens to send" />
                                            <Dropdown
                                                placeholder='Select Receipient'
                                                fluid
                                                selection
                                                onChange={this.handleOnChange}
                                                options={this.state.users}
                                                style={{ margin: "10px",width:800 }}
                                            />
                                            <Button icon labelPosition="right" style={{ margin: "10px" }} onClick={this.sendTokens} >Send
       <Icon className="add user" />
                                            </Button>

                                        </div>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>
<NotificationContainer/>
                           
                        </div>




                    }
                </Segment>


        )
    }
}

export default AccountDetails;