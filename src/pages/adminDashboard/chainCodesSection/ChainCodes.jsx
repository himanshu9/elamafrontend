import React from "react";
import ReactTable from "react-table";

const columns = [
    {
        Header: "Recipent",
        accessor: "recipient"
    },
    {
        Header: "Payment",
        accessor: "payment"
    },
    {
        Header: "Date",
        accessor: "date"
    }
];

const ChainCodes = () => (
    <div>
        <ReactTable columns={columns} style={{marginLeft: "5vw"}}  />
    </div>
);

export default ChainCodes;