import React from "react";
import {Segment, Menu} from "semantic-ui-react";
import Configuration from "./Configuration";
import ExistingChannels from "./ExistingChannels";

class Channels extends React.Component {

    state = {
        activeItem: "configuration"
    }

    handleClick = (event, {name}) => {
        this.setState({activeItem: name})
    }

    render () {
        const {activeItem} = this.state;

        const segmentStyles = {
            marginLeft: "5vw"
        }

        return (
            <Segment style={segmentStyles} raised >
            <Menu widths={2} pointing secondary >
            <Menu.Item name="configuration" 
             active={activeItem === "configuration"} 
             onClick={this.handleClick} 
             color = {activeItem === "configuration" ? "blue" : "black"}
            >Configuration
           </Menu.Item>
           <Menu.Item name="existingChannels" 
           active={activeItem === "existingChannels"} 
           onClick={this.handleClick} 
           color = {activeItem === "existingChannels" ? "blue" : "black"}
           >Existing Channels
           </Menu.Item>
           </Menu>
           {activeItem === "configuration" && <Configuration/> }
           {activeItem === "existingChannels" && <ExistingChannels/> }
         </Segment>
        )
    }
}

export default Channels;