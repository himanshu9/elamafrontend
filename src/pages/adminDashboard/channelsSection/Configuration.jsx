import React from "react";
import {Card, Image} from "semantic-ui-react";

class Configuration extends React.Component {
    render () {
        return (
            <Card.Group centered >
                <Card style={{marginRight: "7vw"}} >
                <Image src='https://robohash.org/V3R.png?set=set3&size=150x150' wrapped ui={false} />
                <Card.Content>
                <Card.Header>Create Channel</Card.Header>
                </Card.Content>
               
                </Card>
                <Card>
                <Image src='https://robohash.org/V3R.png?set=set3&size=150x150' wrapped ui={false} />
                <Card.Content>
                <Card.Header>Join Channel</Card.Header>
                </Card.Content>
                </Card>
            </Card.Group >
        )
    }
};

export default Configuration;