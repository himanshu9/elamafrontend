import React from "react";
import ReactTable from "react-table";

const columns = [
    {
        Header: "Recipent",
        accessor: "recipient"
    },
    {
        Header: "Payment",
        accessor: "payment"
    },
    {
        Header: "Date",
        accessor: "date"
    }
];

const ExistingChannels = () => (
    <div>
        <ReactTable columns={columns} />
    </div>
);

export default ExistingChannels;