import React from "react";
import {Steps} from "antd";
import api from "../../../api/api";
import Step0 from "./steps/Step0";
import Step1 from "./steps/Step1";
import Step2 from "./steps/Step2";
import Step3 from "./steps/Step3";
import Step4 from "./steps/Step4";
import Step5 from "./steps/Step5";
import Step6 from "./steps/Step6";
import Step7 from "./steps/Step7";
import End from "./steps/End"
import Loader from './../../loader';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class ConfigurationSteps extends React.Component {

    componentDidMount() {
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.configuration(headers)
        .then(data => {
          if(data.message === 'Invalid or expired token'){
            localStorage.removeItem('token');
             this.props.history.push('/');
             window.location.reload();
        
            }
            console.log(data,"data")
            const arr = ["orgOne", "orgTwo", "channel", "joinOrgOne", "joinOrgTwo", "chaincodeOrgOne", "chaincodeOrgTwo", "instantiate"];
            arr.map((arg) => {
              if(data[arg]){
                  if(arg === "instantiate") {
                    this.setState({
                        currentStep:this.state.currentStep + 3
                    })
                  } else {
                    this.setState({
                        currentStep:this.state.currentStep + 1
                    })
                  }
              }
            })
        })
    }

    state = {
        currentStep: 0,
        loading:false
    }

    decideStatus = () => {
       this.setState({...this.state, currentStep: this.state.currentStep + 1});
      // Handles NavDropDown Menu Name (Org1 or Org2) 
       this.props.handleNavDropdown(this.state.currentStep);
    }

    toggleLoading = ()=>{
       
        this.setState({
            loading:!this.state.loading
        })
    }

    render () {
       
        const {currentStep} = this.state;

        const stepStyles = {
             marginLeft: "5vw",
             marginTop: "5vh"
        }

        const segmentStyles = {
            position: "fixed",
            marginLeft: "30vw",
            marginTop: "-40vh"
        }

        

        return (
          <div>
          {
              !this.state.loading ? 
              <div>
              
          
              <Steps style={stepStyles} direction="vertical" current={this.state.currentStep}  >
                <Steps.Step  toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
                <Steps.Step toggleLoading={this.toggleLoading}/>
            </Steps>
          <div style={segmentStyles} >
              {currentStep === 0 && <Step0  history={this.props.history} decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading} />}
              {currentStep === 1 && <Step1 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep === 2 && <Step2 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep === 3 && <Step3 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep === 4 && <Step4 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading} /> }
              {currentStep === 5 && <Step5 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep === 6 && <Step6 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep === 7 && <Step7 decideStatus={this.decideStatus}   toggleLoading={this.toggleLoading}/> }
              {currentStep > 7 && <End/>}
          </div>
       
                    <NotificationContainer/>
                    </div>
                    : <Loader/>
          }
          </div>
        )
    }
};

export default ConfigurationSteps;