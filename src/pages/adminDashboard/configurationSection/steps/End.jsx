import React from "react";
import {Segment, Button} from "semantic-ui-react";
import api from "../../../../api/api";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class End extends React.Component {

  

    render () {
        return (
            <Segment raised >
            
            <h1>Configuration Done</h1>
            <h2>Now you can add your wallet ,mint tokens and send tokens.</h2>
            </Segment>
        )
    }
}

export default End;