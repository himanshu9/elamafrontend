import React from "react";
import {Segment, Button} from "semantic-ui-react";
import api from "../../../../api/api";
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


class Step0 extends React.Component {

    toServer = () => {
        this.props.toggleLoading();
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.firstDone(headers)
        .then(data => {
            console.log(data,"step data");
           if (data.success) {
               this.props.decideStatus()
               this.props.toggleLoading();
               NotificationManager.success(data.message);
           } else {
           
            if(data.message === 'Invalid or expired token'){
            localStorage.removeItem('token');
             this.props.history.push('/');
             window.location.reload();
        
            }
            this.props.toggleLoading();
               NotificationManager.error(data.message);
               
           }


        }).catch((err)=>{
            console.log(err,"step data");
            this.props.toggleLoading();
           
        })

    }
    

    render () {
        return (
            <Segment raised >
            <NotificationContainer/>
            <h1>Create Org 1 User</h1>
            <Button  onClick={this.toServer} primary style={{marginTop: "30px"}} >Create</Button>
            </Segment>
    
        )
    }
}

export default Step0;