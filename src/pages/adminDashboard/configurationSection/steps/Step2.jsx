import React from "react";
import {Segment, Button} from "semantic-ui-react";
import api from "../../../../api/api";

import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class Step2 extends React.Component {

    toServer = () => {
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        this.props.toggleLoading();
        api.platform.channelCreated(headers)
        .then(data => {
           if (data.success) {
               this.props.decideStatus()
               this.props.toggleLoading();
               NotificationManager.success(data.message);
           } else {
            
               this.props.toggleLoading();
               NotificationManager.error(data.message);
           }

        }).catch((err)=>{
            
            this.props.toggleLoading();
           
        })

    }


    render () {
        return (
            <Segment raised >
            <NotificationContainer/>
            <h1>Create Channel</h1>
            <Button onClick={this.toServer} primary style={{marginTop: "30px"}} >Create</Button>
            </Segment>
    
        )
    }
}

export default Step2;