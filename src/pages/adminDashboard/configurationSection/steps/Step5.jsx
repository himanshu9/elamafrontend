import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import React from "react";
import {Segment, Button} from "semantic-ui-react";
import api from "../../../../api/api";

class Step5 extends React.Component {

    toServer = () => {
        this.props.toggleLoading();
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.chaincode1(headers)
        .then(data => {
           if (data.success) {
               this.props.decideStatus()
               this.props.toggleLoading();
               NotificationManager.success(data.message);
           } else {
            
               this.props.toggleLoading();
               NotificationManager.error(data.message);
           }

        }).catch((err)=>{
           
            this.props.toggleLoading();
           
        })

    }

    render () {
        return (
            <Segment raised >
            <NotificationContainer/>
            <h1>Install Chaincodes by Org 1</h1>
            <Button onClick={this.toServer} primary style={{marginTop: "30px"}} >Install</Button>
            </Segment>
        )
    }
}

export default Step5;