import React from "react";
import { Grid, Segment, Menu } from "semantic-ui-react";
import ReactTable from 'react-table';
import { Button, Header, Image, Modal, Icon, Input, Dropdown } from "semantic-ui-react";
import api from "../../api/api";
import Loader from '../loader';

import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';

class AccountDetails extends React.Component {
    Accounts
    state = {
        balance: "0",
        activeItem: "account",
        loading: false,
        mintModel: false,
        sendModel: false,
        users: [],
        history:[],
        toAddress:''

    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
      
        this.getUsers(headers);

    }

    getHistory = (user) => {
        this.setState({
            loading:true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.user.getTxHistory(headers,user.walletAddress)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        history: data.data

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getBalance = (user) => {

        console.log(user)
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.user.getUserBalance(headers,user.walletAddress)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        balance: data.balance

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getUsers = (headers) => {
        api.platform.allUsers(headers)
            .then(data => {

                if (data.success) {

                    let users = data.data;
                   
                    this.setState({
                        loading: false,
                        users: users

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    handleClick = (event, { name }) => {
        this.setState({ activeItem: name })
        if(name=== 'history') {
            this.getHistory();
        }
        
    }

   

   

    toggleMintMAccountsodal = () => {
        this.setState({
            mintModel: !this.state.mintModel
        })
    }

   

  
    render() {

        console.log("history",this.state.history)

        const segmentStyles = {
            marginLeft: "5vw"
        }

        let { activeItem } = this.state

       
        const columns = [
            {
                Header: "WalletAddress",
                accessor: "walletAddress"
            },
            {
                Header: "Email",
                accessor: "email"
            }
        ];

        const historyColumns = [
            {
                Header: "To",
                accessor: "to"
            },
            {
                Header: "Action",
                accessor: "action"
            },
            {
                Header: "Amount",
                accessor: "amount"
            },
            {
                Header: "TxId",
                accessor: "txid"
            }
        ];

        


        const tableStyles = {
            marginLeft: "2vw"
        }



        return (


            this.state.loading ? <Loader /> :
                <Segment style={segmentStyles} raised >
                <ReactTable 
               
                getTrProps={(state, rowInfo) => {
                    if (rowInfo && rowInfo.row) {
                      return {
                        onClick: (e) => {
                          console.log("selected",rowInfo.original)
                          this.setState({
                            balanceModel:true
                          })
                          this.getBalance(rowInfo.original)
                          this.getHistory(rowInfo.original)
                        },
                        style: {
                          background: rowInfo.index === this.state.selected ? '#00afec' : 'white',
                          color: rowInfo.index === this.state.selected ? 'white' : 'black'
                        }
                      }
                    }else{
                      return {}
                    }
                  }} 
                  
                  data={this.state.users} columns={columns} style={tableStyles} defaultPageSize={11} />



                        <div>

                     
                            <Modal open={this.state.balanceModel}>
                                <Modal.Header>User Balance</Modal.Header>
                                <Icon className="close" onClick={()=>(this.setState({balanceModel:false}))}/>
                                <Modal.Content image>
                                  
                                    <Modal.Description>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: 20 }}>

                                         <h2>Balance :{this.state.balance}</h2>

                                         <ReactTable data={this.state.history} columns={historyColumns} style={tableStyles} defaultPageSize={5} />

                                        </div>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>





                          

                            <NotificationContainer/>
                           
                        </div>




                    }
                </Segment>


        )
    }
}

export default AccountDetails;