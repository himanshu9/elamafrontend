import React from "react";
import {Button, Icon} from "semantic-ui-react";

import AccountDetails from "./balance";
import api from "../../../api/api";
import Loader from './../../loader';
import Pin from '../../pin';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';


class Account extends React.Component {

  componentDidMount(){
      this.setState({
          detailsLoading:true
      })
      const headers = {
        headers: {
          'x-auth-token': localStorage.getItem('token'),
        }
       
      };
    api.user.getDetails(headers)
    .then(data => {
       if (data.success) {
        this.props.handleUserDetails(data.data)
        if(data.data.wallet){
            this.setState({
                isPriorAccount:true,
                walletAddress:data.data.walletAddress
            })
        } else {
            this.setState({
                isPriorAccount:false
            })
        }

        this.setState({
            userData:data,
            detailsLoading:false
        })
        
       } else {
        this.setState({
            detailsLoading:false
        })
       }

    }).catch((err)=>{
        this.setState({
            detailsLoading:false
        })
    })
  }

    state = {
        isPriorAccount: false,
        isFormOpen: false,
        detailsLoading:false,
        walletAddress:'',
        userData:{
            wallet:false
        },
        pinModal:false
    }

    makeTransaction = (pin)=>{
        let body={
            pincode:pin
        }
        this.setState({
            detailsLoading:true,
            pinModal:false,
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.platform.createUserAccount(body,headers)
    .then(data => {
       if (data.success) {      
          
        api.user.getDetails(headers)
        .then(data => {
           if (data.success) {
             
            if(data.data.wallet){
                this.setState({
                    pinModal:false,
                    isPriorAccount:true,
                    walletAddress:data.data.walletAddress
                })
            } else {
                this.setState({
                    pinModal:false,
                    isPriorAccount:false
                })
            }
    
            this.setState({
                pinModal:false,
                userData:data,
                detailsLoading:false
            })
            NotificationManager.success(data.message);
           } else {
            this.setState({
                pinModal:false,
                detailsLoading:false
            })
           
           }
    
        }).catch((err)=>{
            this.setState({
                detailsLoading:false
            })
        })
       } else {
        this.setState({
            detailsLoading:false
        })
        NotificationManager.error(data.message);
       }

    }).catch((err)=>{
        this.setState({
            pinModal:false,
            detailsLoading:false
        })
    })
    }

    openPin = ()=>{
        this.setState({
            pinModal:true
        })
    }
    render () {
        const {isFormOpen, isPriorAccount} = this.state;

        return (
          <div>
          {
              this.state.detailsLoading ? <Loader/> :
              <div>
              {!isPriorAccount && <div>
               <Button icon labelPosition="right" onClick = {this.openPin} style={{marginTop: "10px"}} >Create Account
              <Icon className="add user"/>
              </Button>
           
              </div>
              }
              {isPriorAccount && <AccountDetails walletAddress={this.state.walletAddress}/>}
              </div>  
          }
          <Pin pinModal={this.state.pinModal} pinHeader={"Set a pin for your account"} makeTransaction={this.makeTransaction}/>
          <NotificationContainer/>
          </div>  
        )
    }
}

export default Account;