import React from "react";
import { Grid, Segment, Menu } from "semantic-ui-react";
import ReactTable from 'react-table';
import { Button, Header, Image, Modal, Icon, Input, Dropdown } from "semantic-ui-react";
import api from "../../../api/api";
import Loader from './../../loader';
import Pin from '../../pin';
import {NotificationContainer, NotificationManager} from 'react-notifications';
import 'react-notifications/lib/notifications.css';
import Users from '../../adminDashboard/users'

class AccountDetails extends React.Component {

    state = {
        balance: "0",
        activeItem: "account",
        loading: false,
        mintModel: false,
        sendModel: false,
        users: [],
        history:[],
        pinModal:false

    }

    componentDidMount() {
        this.setState({
            loading: true
        })
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        this.getBalance(headers);
        this.getUsers(headers);

    }

    getHistory = () => {
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.user.getTxHistory(headers)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        history: data.data

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getBalance = (headers) => {
        api.user.getBalance(headers)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        balance: data.balance

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    getUsers = (headers) => {
        api.platform.allUsers(headers)
            .then(data => {

                if (data.success) {

                    let users = data.data;
                    console.log(users)
                    let modifiedUsers = [];

                    users.map((user) => {
                        let modifiedUser = {};
                        modifiedUser.key = user.email;
                        modifiedUser.text = user.email;
                        modifiedUser.value = user.walletAddress;

                        modifiedUsers.push(modifiedUser);
                    })
                    this.setState({
                        loading: false,
                        users: modifiedUsers

                    })
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })
    }

    handleClick = (event, { name }) => {
        this.setState({ activeItem: name })
        if(name=== 'history') {
            this.getHistory();
        }
        
    }

    mintTokens = () => {

        console.log(this.state.mintTokensNumber)
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        this.setState({
            loading: true
        })
        let body = {
            amount: this.state.mintTokensNumber
        }


        api.platform.mintTokens(body,headers)
            .then(data => {

                if (data.success) {
                    this.setState({
                        loading: false,
                        mintModel: false
                    })
                    this.getBalance();
                } else {
                    this.setState({
                        loading: false
                    })
                }

            }).catch((err) => {
                this.setState({
                    loading: false
                })
            })

    }

    makeTransaction= (pin)=>{
        this.setState({
            pinModal:false,
            loading:true
        })


        this.setState({
            loading:true
        })
        if(this.state.sendTokensNumber <= 0) {
            NotificationManager.error("Token number should be greater than zero");
        } else {

        let body = {
            amount: this.state.sendTokensNumber,
            toAddress:this.state.toAddress,
            pincode:pin
        }
        const headers = {
            headers: {
              'x-auth-token': localStorage.getItem('token'),
            }
           
          };
        api.user.sendPayment(body,headers)
        .then(data => {

            if (data.success) {
                this.setState({
                    loading: false,
                    sendModel: false
                })
                this.getBalance(headers);
                NotificationManager.success(data.message);
            } else {
                this.setState({
                    loading: false
                })
                NotificationManager.error(data.message);
            }

        }).catch((err) => {
            this.setState({
                loading: false
            })
           
        })

    }
    }

    sendTokens = () => {

        this.setState({
            pinModal:true
        })
    }

    toggleMintModal = () => {
        this.setState({
            mintModel: !this.state.mintModel
        })
    }

    sendModel = () => {
        this.setState({
            sendModel: !this.state.sendModel
        })
    }

    changeMintTokens = (e) => {
        this.setState({
            mintTokensNumber: e.target.value
        })
    }

    changeSendTokens = (e) => {
        this.setState({
            sendTokensNumber: e.target.value
        })
    }

    handleOnChange = (e, data) => {
        console.log(data.value);
        this.setState({
            toAddress:data.value
        })
     }
    render() {

        console.log("users",this.state.users)

        const segmentStyles = {
            margin: "5vw"
            
        }

        let { activeItem } = this.state
        const columns = [
            {
                Header: "To",
                accessor: "to",
                // Cell: ({ value }) => {
                //     if(!!value) {
                //         return value;
                //     } else {
                //         return "Elamachain"
                //     }
                //   }, // Custom cell components!
            },
            {
                Header: "Action",
                accessor: "action"
            },
            {
                Header: "Amount",
                accessor: "amount"
            },
            {
                Header: "TxId",
                accessor: "txid"
            }
        ];

        const tableStyles = {
            marginLeft: "2vw"
        }



        return (


            this.state.loading ? <Loader /> :
                <Segment style={segmentStyles} raised >
                    <Menu widths={3} pointing secondary >
                        <Menu.Item name="account"
                            active={activeItem === "account"}
                            onClick={this.handleClick}
                            color={activeItem === "account" ? "blue" : "black"}
                        >Wallet
   </Menu.Item>
                        <Menu.Item name="history"
                            active={activeItem === "history"}
                            onClick={this.handleClick}
                            color={activeItem === "history" ? "blue" : "black"}
                        >Transaction History
   </Menu.Item>

   <Menu.Item name="users"
                            active={activeItem === "users"}
                            onClick={this.handleClick}
                            color={activeItem === "users" ? "blue" : "black"}
                        >All Users
   </Menu.Item>
                    </Menu>
                    {activeItem === "users" && <Users/>}

                    {activeItem === "history" && <ReactTable data={this.state.history} columns={columns} style={tableStyles} defaultPageSize={11} />}
                    {activeItem === "account" &&




                        <div>

                            <h1 style={{ color: "green" }} >Wallet Address:</h1>
                            <h4 style={{ color: "green" }} >{this.props.walletAddress}</h4>




                            <h1 style={{ color: "green" }} >Balance:</h1>
                            <h4 style={{ color: "green" }} >{this.state.balance}</h4>

                          



                            <Modal  open={this.state.sendModel} trigger={<Button icon labelPosition="right" style={{ marginTop: "10px" }}  onClick={()=>(this.setState({sendModel:true}))}>Send Tokens
                            <Icon className="add user" />
                                                   </Button>}>
                                
                                <Modal.Header>Send Tokens</Modal.Header>
                                <Icon className="close" onClick={()=>(this.setState({sendModel:false}))}/>
                                <Modal.Content image>
                                    
                                    <Modal.Description>
                                        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: 20 }}>


                                            <Input size='huge' style={{width:'800px',borderWidth:'5px'}} onChange={this.changeSendTokens} value={this.state.sendTokensNumber} placeholder="Enter Number of tokens to send" />
                                            <Dropdown
                                            onChange={this.handleOnChange}
                                          
                                                placeholder='Select Receipient'
                                                fluid
                                                selection
                                                options={this.state.users}
                                                style={{ margin: "10px",width:'800px' }}
                                            />
                                            <Button icon labelPosition="right" style={{ margin: "10px" }} onClick={this.sendTokens} >Send
       <Icon className="add user" />
                                            </Button>

                                        </div>
                                    </Modal.Description>
                                </Modal.Content>
                            </Modal>

                            <Pin pinModal={this.state.pinModal} pinHeader={"Enter Your Wallet Pin To Send Tokens"} makeTransaction={this.makeTransaction}/>
                        </div>




                    }
                    <NotificationContainer/>
                </Segment>


        )
    }
}

export default AccountDetails;