import React from "react";
import {Segment} from "semantic-ui-react";
import ReactTable from 'react-table';


class PaymentHistory extends React.Component {
    render () {
        const columns = [
            {
                Header: "Recipent",
                accessor: "recipient"
            },
            {
                Header: "Payment",
                accessor: "payment"
            },
            {
                Header: "Date",
                accessor: "date"
            }
        ];

        const segmentStyles = {
            marginLeft: "5vw",
            marginTop: "5vh"
        }

        return(
            
            <Segment style={segmentStyles} raised >
                <ReactTable columns={columns} pageSize="5"/>
            </Segment>
        
        )
    }
}

export default PaymentHistory;