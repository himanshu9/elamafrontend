import React from "react";
import {Segment, Menu} from "semantic-ui-react";
import PaymentHistory from "./PaymentHistory";
import SendPayment from "./SendPayment";

class Payments extends React.Component {

    state = {
        activeItem: "paymentHistory"
    }

    handleClick = (e, {name}) => {
        this.setState({activeItem: name})
    }

    render () {
        const {activeItem} = this.state;

        const segmentStyles = {
            marginLeft: "5vw"
        }

        return (
            <div>
            <h1>Payments</h1>
           <Segment raised style={segmentStyles} >
           <Menu widths={2} pointing secondary >
             <Menu.Item name="paymentHistory"  
              active={activeItem === "paymentHistory"} 
              onClick={this.handleClick} 
              color = {activeItem === "paymentHistory" ? "violet" : "black"}
              >Payment History
              </Menu.Item>
             <Menu.Item name="newPayment" active={activeItem === "newPayment"} 
             onClick={this.handleClick}
             color = {activeItem === "newPayment" ? "violet" : "black"}
             >New Payment
             </Menu.Item>
            </Menu>
           </Segment>
           {activeItem === "paymentHistory" && <PaymentHistory/>}
            {activeItem === "newPayment" && <SendPayment/>}
           </div>
        )
    }
}

export default Payments;