import React from "react";
import {Form, Button, Grid, Segment, Input, Icon} from "semantic-ui-react";

class SendPayment extends React.Component {
    render () {

        const segmentStyles = {
            marginTop: "3vw"
        }

        return (
            <Grid>
                <Grid.Column width={5} ></Grid.Column>
                <Grid.Column width={6} >
                <Segment  raised textAlign={"left"} clearing style={segmentStyles}  >
                    <Form>
                        <Form.Field>
                            <label>Recipient Name:</label>
                            <input type="text"
                            placeholder="Enter Recipient Name"
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>Amount:</label>
                            <Input placeholder="Enter Amount" iconPosition="left"
                            icon={<Icon className="rupee sign" />}
                            type = "number"
                            />
                         
                        </Form.Field>
                        <Button primary floated="right" >Submit</Button>
                    </Form>
                </Segment>
                </Grid.Column>
            </Grid>
        )
    }
}

export default SendPayment;