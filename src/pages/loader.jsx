import Loader from 'react-loader-spinner'
import React from "react";


export default class App extends React.Component {
 //other logic
   render() {
    return(
    <div style={{display:'flex',flexDirection:'column'}}>
    <Loader 
    type="Puff"
    color="#00BFFF"
    height="100"	
    width="100"
 />
  <h2 style={{color:'#000'}}>Processing...</h2>
  </div>
    );
   }
}