import React from "react";
import { Menu, Dropdown} from "semantic-ui-react";



const Navbar = ({isOrg2, history,userDetails}) => (
  
    <Menu className="navigation" fixed="top" size="massive" >
            <Menu.Item header >
                <h1>Elmachain</h1>
            </Menu.Item>
            <Menu.Item className="right floated">
            <h4 style={{color:'#fff',marginRight:10}}>{userDetails.username}</h4>
        <Dropdown pointing="top right" text={isOrg2 ? "Org 2" : "Org 1"} style={{color:"white"}} >
        <Dropdown.Menu  >
          <Dropdown.Item text="Org 1" icon="users" />
          <Dropdown.Item text="Org 2" icon="users" />
          <Dropdown.Item icon="sign out" text="Log Out" 
          onClick = {() => {
              localStorage.removeItem('token');
              history.push('/');
              window.location.reload();
          }}
          ></Dropdown.Item>
         </Dropdown.Menu>
        </Dropdown>
         </Menu.Item>
    </Menu>
)

export default Navbar;