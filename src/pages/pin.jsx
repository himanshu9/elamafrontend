import React from "react";
import PropTypes from "prop-types";
import { Button, Header, Image, Modal, Icon, Input, Dropdown } from "semantic-ui-react";
import PinInput from "react-pin-input";



class SetPin extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            pin: '',
            credsOpen: false
        }
        this.ele = React.createRef()
    }

    handlePin = () => {
        if (!!this.state.pin) {
            this.ele.clear()
            this.setState({
                pin: ''
            })
            this.props.makeTransaction(this.state.pin)
        } else {
            //this.notifyError("Please enter your pin")
        }

    }

    pinModal = () => {
        const { classes } = this.props;
        return (
            <div>

                <div className="verticalFlex" >
                  
                    <div className="centerFlex">
                        <PinInput
                            ref={(n) => this.ele = n}
                            length={4}
                            secret
                            type="numeric"
                            style={{ padding: '10px' }}
                            inputStyle={{ borderColor: 'red' }}
                            inputFocusStyle={{ borderColor: 'blue' }}
                            onComplete={(value, index) => {
                                this.setState({
                                    pin: value
                                })
                            }}
                        />
                    </div>
                 
                </div>


            </div>


        );
    };





    render() {
        return (
            <div className="container">

                <Modal open={this.props.pinModal}>
                    <Modal.Header>{this.props.pinHeader}</Modal.Header>
                    <Modal.Content image>
                        
                        <Modal.Description>
                            <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', margin: 20 }}>


                                {this.pinModal()}
                                <Button   style={{ margin: "10px" }} onClick={this.handlePin} >Proceed
      
                                </Button>

                            </div>
                        </Modal.Description>
                    </Modal.Content>
                </Modal>
              
            </div>
        );
    }
}

SetPin.propTypes = {
    classes: PropTypes.object.isRequired
};

export default SetPin;
